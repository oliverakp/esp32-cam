from django.urls import path
from django.contrib.auth import views as AuthViews
from . import views
from .forms import LoginForm
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login/', AuthViews.LoginView.as_view(template_name='accounts/auth/login.html' ,redirect_authenticated_user=True, authentication_form=LoginForm),name='accounts.login'),
    path('logout/', AuthViews.LogoutView.as_view(template_name='accounts/auth/logout.html' ,next_page='accounts.login'),name='accounts.logout'),
    path('register/', views.register, name='register'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)