from django import forms
from django.contrib.auth.forms import AuthenticationForm

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Nom d\'utilisateur : ", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'joseph.alao',
    }))
    password = forms.CharField(label="Mot de passe : ", widget=forms.PasswordInput(attrs={
        'class' : 'form-control',
        'placeholder' : '**************',
    }))
    
class UploadFileForm(forms.Form):
    file = forms.FileField()