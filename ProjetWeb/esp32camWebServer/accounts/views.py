from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import UploadFileForm
from django.http import HttpResponse
from django.http import Http404,JsonResponse, HttpResponseBadRequest
from .models import *
import json
from django.shortcuts import get_object_or_404


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('manager/customers')
    else:
        form = UserCreationForm()
        return render(request, 'accounts/auth/register.html', {'form': form})

