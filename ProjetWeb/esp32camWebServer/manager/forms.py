from django import forms
from django.forms import ModelForm
from .models import *


class DeviceForm(ModelForm):
    
    class Meta:
        model = Device
        fields = ("name","user","model")
    
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': '...'}))
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.Select(attrs={ 'class':'form-select', 'name':"quiz_id", 'id':"id_quiz"}), label='Device')
    model = forms.CharField(widget=forms.HiddenInput(), initial='Device') 
   