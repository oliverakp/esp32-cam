from django.contrib import admin
from django.contrib.admin import AdminSite

# Register your models here.
from .models import Device, Screen, Data

class AdminDevice(admin.ModelAdmin):
    search_fields = ['name', 'user']
    list_display = ('name', 'user')

class AdminScreen(admin.ModelAdmin):
    search_fields = ['name', 'device']
    list_display = ('name', 'device')

class AdminData(admin.ModelAdmin):
    search_fields = ['tension', 'intensity', 'temperature', 'unity', 'time']
    list_display = ('tension', 'intensity', 'temperature', 'unity', 'time')


AdminSite.site_header = "ScreenReader SuperAdmin"
admin.site.register(Device, AdminDevice)
admin.site.register(Screen, AdminScreen)
admin.site.register(Data, AdminData)