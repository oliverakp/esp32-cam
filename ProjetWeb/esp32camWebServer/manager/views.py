from django.shortcuts import render, get_object_or_404, redirect
import requests
from django.conf import settings
from django.http import HttpResponse, Http404, FileResponse
from django.core.files.storage import FileSystemStorage
import os
from django.http import StreamingHttpResponse
import io
import time
from .forms import *
from django.apps import apps
from django.utils.module_loading import import_string
from django.contrib import messages
import random
from django.utils import timezone
import datetime
import matplotlib.pyplot as plt
import numpy as np
from django.http import StreamingHttpResponse
from PIL import Image

#import picamera

# Create your views here.

def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

def dashboard(request):
    pass

def customers(request):
    data = {
        'deviceForm':DeviceForm(),
        'devices': Device.objects.all(),
    }
    return render(request, 'quiz/teachers.html', data)

def devices(request):
    """
    tensions = Data.objects.filter(tension__gt=0)[:1000]
    intensity = Data.objects.filter(intensity__gt=0)[:1000]
    temperatures = Data.objects.filter(temperature__gt=0)[:1000]
    
    tensions_data = tensions.values_list('tension', flat=True)[::10]
    intensity_data = intensity.values_list('intensity', flat=True)[::10]
    temperatures_data = temperatures.values_list('temperature', flat=True)[::10]
    
    t_data = [item for item in tensions_data]
    i_data = [item for item in intensity_data]
    temp_data = [item for item in temperatures_data]
    
    timesTensions = tensions.values_list('time', flat=True)[::10]
    timesIntensity = intensity.values_list('time', flat=True)[::10]
    timesTemperatures = temperatures.values_list('time', flat=True)[::10]

    times_t_data = [item for item in timesTensions]    
    times_i_data = [item for item in timesIntensity]
    times_temp_data = [item for item in timesTemperatures]
    
    fig, axs = plt.subplots(3, 1)
    axs[0].plot(times_t_data, t_data)
    axs[0].set_title('Tension (V)')
    axs[0].grid()
    
    axs[1].plot(times_i_data, i_data)
    axs[1].set_title('Intensity (A)')
    axs[1].grid()
    
    axs[2].plot(times_temp_data, temp_data)
    axs[2].set_title('Temperature (°C)')
    axs[2].grid()
    fig.tight_layout()
    
    plt.show()
    """ 
    """
    plt.figure()
    plt.plot(times_t_data, t_data)
    plt.title("Tension")
    plt.show()
    
    plt.figure()
    plt.plot(times_i_data, i_data)
    plt.title("Intensity")
    plt.show()
    
    plt.figure()
    plt.plot(times_temp_data, temp_data)
    plt.title("Temperature")
    plt.show()
    """
    #print(times)
    data = {
        'deviceForm':DeviceForm(),
        'devices': Device.objects.all(),
    }
    return render(request, 'manager/devices.html', data)

def screens(request, deviceId):
    data = {
        'deviceForm':DeviceForm(),
        'scr': Device.objects.all(),
        'device' : Device.objects.get(id=int(deviceId))
    }
    return render(request, 'manager/screens.html', data)

def create(request):
    if request.method == 'POST':
        model, modelForm = getModelAndForm(request.POST["model"], 'quiz')

        if request.FILES is not None:
            form = modelForm(request.POST,request.FILES)
        else:
            form = modelForm(request.POST)
        if form.is_valid():
            instance = form.save()
            messages.success(request, 'Added successfully!')
        else:
            errors = form.errors
            for field, error in errors.items():
                messages.error(request, f'{field}: {error}')
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse("Opps !!! Quelque chose s'est mal passée. Veuillez recommencer.")

def detailAjax(request):
    if is_ajax(request=request):
        if request.method == "POST": 
            model, modelForm = getModelAndForm(request.POST["model"], 'quiz')
            id = request.POST['model_id'] 
            obj= get_object_or_404(model, id=id)
            form = modelForm(instance=obj)
        data = {
            'form':form,
        }
        template = 'quiz/update/update'+ request.POST["model"] +'Modal.html'
        return render(request, template, data)
    else:
        return HttpResponse("Opps !!! Quelque chose s'est mal passée. Veuillez recommencer.")

def update(request):
    if request.method == 'POST':
        model, modelForm = getModelAndForm(request.POST["model"], 'quiz')
        id = request.POST['model_id'] 
        obj= get_object_or_404(model, id=id)
        #form = modelForm(request.POST, instance=obj)
        if request.FILES is not None:
            form = modelForm(request.POST,request.FILES, instance=obj)
        else:
            form = modelForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            messages.success(request, 'Updated successfully!')
        else:
            errors = form.errors
            for field, error in errors.items():
                messages.error(request, f'{field}: {error}')
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse("Opps !!! Quelque chose s'est mal passée. Veuillez recommencer.")
  
def delete(request):
    if request.method == 'POST':
        model, modelForm = getModelAndForm(request.POST["model"], 'quiz')
        obj = get_object_or_404(model, pk=request.POST["model_id"])
        obj.delete()
        messages.success(request, 'Deleted successfully!')
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse("Opps !!! Quelque chose s'est mal passée. Veuillez recommencer.")

def getModelAndForm(model_name, app_name):
    # Get the model class from the app registry
    modelClass = apps.get_model(app_name, model_name)

    # Get the ModelForm class for the model, if it exists
    form_path = app_name + '.forms.' + model_name + 'Form'
    form = import_string(form_path)

    return modelClass, form

def generate(request):
    start_date = datetime.datetime(2023, 2, 14, 15, 42, tzinfo=timezone.utc)
    stop_date = datetime.datetime(2023, 2, 15, 15, 42, tzinfo=timezone.utc)
    #for i in range (1,random.randint(5,9)):
    screen = Screen.objects.get(id = 1)
    while start_date < stop_date:
        data = Data(
            tension = random.uniform(8, 14), 
            unity = "V",
            time= start_date,
            screen = screen
        )
        data.save()
        
        start_date += timezone.timedelta(seconds=4) 
        
        data = Data(
            intensity = random.uniform(0.2, 5), 
            unity = "A",
            time= start_date,
            screen = screen
        )
        data.save()
        
        start_date += timezone.timedelta(seconds=4) 
        
        data = Data(
            temperature = random.randint(30, 32), 
            unity = "°C",
            time= start_date,
            screen = screen
        )
        data.save()
        
        start_date += timezone.timedelta(seconds=4) 
    return HttpResponse("Generated !")
"""
    image_name = 'temp1.PNG'
    
    # Get the full path to the image file.
    media_root = settings.MEDIA_ROOT
    image_path = os.path.join(media_root, image_name)
    
    url = 'https://app.nanonets.com/api/v2/OCR/Model/291ce5a1-ec0d-4ab2-897e-3e3784b5852a/LabelFile/'

    data = {'file': open(image_path, 'rb')}

    response = requests.post(url, auth=requests.auth.HTTPBasicAuth('15410fdc-b372-11ed-bf6d-72b556e70830', ''), files=data)

    print(response.text)
    
    return HttpResponse("Vou")
    
    def index(request):

        # Initialise la caméra
        camera = picamera.PiCamera()

        # Configure la résolution et la fréquence d'images
        camera.resolution = (640, 480)
        camera.framerate = 24

        # Attend que la caméra se réveille
        time.sleep(2)

        # Génère un flux d'images en continu
        def generate():
            try:
                while True:
                    # Crée un flux d'images en mémoire
                    stream = io.BytesIO()

                    # Capture une image et enregistre-la dans le flux
                    camera.capture(stream, format='jpeg')

                    # Réinitialise le flux pour la lecture
                    stream.seek(0)

                    # Renvoie le contenu de l'image
                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + stream.read() + b'\r\n')

                    # Attend 3 secondes avant la prochaine capture
                    time.sleep(3)

            except KeyboardInterrupt:
                # Ferme la caméra si l'utilisateur interrompt la capture
                camera.close()

        # Renvoie la réponse HTTP avec le flux d'images en continu
        return StreamingHttpResponse(generate(), content_type='multipart/x-mixed-replace; boundary=frame')

"""

def get_stream():
    # ESP32 IP address and port
    url = "http://192.168.100.30:81/stream"
    
    # Continuously read the stream and yield each frame
    while True:
        try:
            # Read the stream
            response = requests.get(url, stream=True)
            bytes_data = bytes()
            
            # Process the response chunk by chunk
            for chunk in response.iter_content(chunk_size=1024):
                bytes_data += chunk
                a = bytes_data.find(b'\xff\xd8')
                b = bytes_data.find(b'\xff\xd9')
                
                # If a valid frame is found, yield it
                if a != -1 and b != -1:
                    jpg = bytes_data[a:b+2]
                    bytes_data = bytes_data[b+2:]
                    img = Image.open(io.BytesIO(jpg))
                    yield (b'--frame\r\n'
                           b'Content-Type: image/jpeg\r\n\r\n' + img.tobytes() + b'\r\n')
        except:
            pass

def stream_view(request):
    return StreamingHttpResponse(get_stream(), content_type='multipart/x-mixed-replace; boundary=frame')