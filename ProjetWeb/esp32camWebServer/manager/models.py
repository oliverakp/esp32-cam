from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class TimespamModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Device(TimespamModel):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    photo = models.FileField(blank=True)
    
class Screen(TimespamModel):
    name = models.CharField(max_length=100)
    device = models.ForeignKey(Device, on_delete=models.CASCADE, null=False)
    photo = models.FileField(blank=True)
    
class Data(models.Model):
    photo = models.FileField(blank=True)
    tension = models.DecimalField(blank=True, null=True, max_digits=3, decimal_places=1, verbose_name='tension')
    intensity = models.DecimalField(blank=True, null=True, max_digits=3, decimal_places=1, verbose_name='intensity')
    temperature = models.IntegerField(blank=True, null=True)
    unity = models.CharField(max_length=2, blank=True, null=True)
    screen = models.ForeignKey(Screen, on_delete=models.CASCADE, null=False)
    time = models.DateTimeField()


