from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('ocr', views.dashboard, name='manager.ocr'),
    path('customers', views.customers, name='manager.customers'),
    path('devices', views.devices, name='manager.devices'),
    path('screens/<str:deviceId>', views.screens, name='manager.screens'),
    path('stream-view', views.stream_view, name='stream.view'),
    
    path('generate', views.generate, name='manager.generate'),
] 

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    