import os
from utils import *

folder_path = "files"  # Path to the folder containing the images

# List all files in the folder
file_list = os.listdir(folder_path)

# Filter image files
image_files = [file for file in file_list if file.endswith(( ".jpeg"))]

image_files = image_files[-100:]

# Print the image files
for image_file in image_files:
    print(image_file)
    recongize_value('files/' + image_file)
    
