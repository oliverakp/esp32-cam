import cv2
import numpy as np
import tensorflow as tf
from tensorflow import keras

# Load the pre-trained MNIST model
model = keras.models.load_model('mnist_model.h5')

def segment_digit(image):
    # Preprocess the image
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # Find contours of the digits
    contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    digit_segments = []
    for contour in contours:
        # Get the bounding rectangle of the contour
        x, y, w, h = cv2.boundingRect(contour)

        # Filter out small bounding rectangles
        if w < 10 or h < 10:
            continue

        # Extract the segment region from the thresholded image
        segment = threshold[y:y+h, x:x+w]

        # Resize the segment to 28x28
        segment = cv2.resize(segment, (28, 28))

        # Preprocess the segment
        segment = segment.astype('float32') / 255.0
        segment = np.expand_dims(segment, axis=0)

        # Predict the digit using the pre-trained model
        prediction = model.predict(segment)
        digit = np.argmax(prediction)

        digit_segments.append(digit)

    return digit_segments

# Load the image containing the 7-segment display
image = cv2.imread('files/esp32-20230515-012830.JPEG')

# Segment the digits
segments = segment_digit(image)

print("Segments detected:", segments)
