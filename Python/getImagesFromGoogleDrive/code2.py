import cv2
import pytesseract

# Set the path to the Tesseract executable
pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'

import os
from utils import *

from utils2 import *

folder_path = "files"  # Path to the folder containing the images

# List all files in the folder
file_list = os.listdir(folder_path)

# Filter image files
image_files = ['files/' + file for file in file_list if file.endswith(( ".jpeg"))]

image_files = image_files[-100:]

for i, img_nm in enumerate(image_files):
    img = cv2.imread(img_nm)
    img = cv2.resize(img, (800, 600))
    gry = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gry = cv2.threshold(gry, 80, 255, cv2.THRESH_BINARY)[1]
    x1, y1, x2, y2 = 283, 200 ,323, 283
    # cv2.rectangle(gry, (x1, y1), (x2+150, y2), (0, 0, 255), 2)
    # cv2.imshow('img', gry)
    # cv2.waitKey(0)
    gry = gry[y1:y2, x1:x2+150]
    im = img[y1:y2, x1:x2+150]
    cv2.imwrite('files/processing/all.jpg', im)
    # cv2.imshow('img', gry)
    # cv2.waitKey(0)
    (h, w) = gry.shape[:2]

    if i == 0:
        thr = gry
    else:
        gry = cv2.resize(gry, (w * 2, h * 2))
        erd = cv2.erode(gry, None, iterations=1)

        if i == len(image_files) - 1:
            thr = cv2.threshold(erd, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
        else:
            thr = cv2.threshold(erd, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    bnt = cv2.bitwise_not(thr)

    # Further preprocessing to enhance digit recognition
    blur = cv2.GaussianBlur(bnt, (5, 5), 0)
    b = cv2.bitwise_not(blur)

    # cv2.imshow('img', b)
    # cv2.waitKey(0)

    # Perform OCR on the preprocessed image
    txt = pytesseract.image_to_string(b, config="--psm 6 digits")
    digits = "".join([t for t in txt if t.isalnum()])
    print("Detected digits:", digits)

    extract1(gry)
    # cv2.imshow("bnt", bnt)
    # cv2.waitKey(0)
