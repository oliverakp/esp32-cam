# import the necessary packages
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
import cv2
import pytesseract
import numpy as np
import os
from utils import *

# Set the path to the Tesseract executable
pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'
# define the dictionary of digit segments so we can identify
# each digit on the thermostat
DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 1, 0): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9
}


folder_path = "files"  # Path to the folder containing the images

# List all files in the folder
file_list = os.listdir(folder_path)

# Filter image files
image_files = [file for file in file_list if file.endswith(( ".jpeg"))]

image_files = image_files[-40:]

# Print the image files
for image_file in image_files:
    # print(image_file)
    # load the example image
    image = cv2.imread('files/' + image_file)
    image = imutils.resize(image, height=600, width=800)
    x1, y1, x2, y2 = 283, 180 ,323, 283 #265
    
    gry = image[y1:y2, x1:x2+150]
    
    gray = cv2.cvtColor(gry, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (7, 7), 0)
    edged = cv2.Canny(blurred, 50, 200, 255)


    edged = cv2.threshold(blurred, 130, 255, cv2.THRESH_BINARY)[1]
    
    # cv2.imshow('img', edged)
    # cv2.waitKey(0)
    
    custom_config = r'--psm 6 -c tessedit_char_whitelist=0123456789'
    txt = pytesseract.image_to_string(edged, config=custom_config)     #"--psm 6 digits"
    
    if contains_chars(txt):
        print('0.0')
    else:
        print(replace_characters(txt))
        
