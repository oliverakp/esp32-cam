import cv2


def extract1(image):
    # image = cv2.imread(image)
    # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    digit_contours = []
    min_area = 100  # Minimum area to consider a contour as a digit
    min_aspect_ratio = 0.6  # Minimum aspect ratio to consider a contour as a digit

    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        aspect_ratio = w / float(h)

        if cv2.contourArea(contour) > min_area and aspect_ratio > min_aspect_ratio:
            digit_contours.append(contour)
    digits = []
    digit_size = (28, 28)  # Desired digit size

    for contour in digit_contours:
        x, y, w, h = cv2.boundingRect(contour)
        digit = binary[y:y+h, x:x+w]  # Crop the digit region
        digit = cv2.resize(digit, digit_size)  # Resize to desired size
        digits.append(digit)
    print(digits)

