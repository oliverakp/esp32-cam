import cv2

DIGITSDICT = {
    (0, 0, 0, 0, 0, 0, 0): " ",
    (1, 1, 1, 1, 1, 1, 0): "0",
    (0, 1, 1, 0, 0, 0, 0): "1",
    (1, 1, 0, 1, 1, 0, 1): "2",
    (1, 1, 1, 1, 0, 0, 1): "3",
    (0, 1, 1, 0, 0, 1, 1): "4",
    (1, 0, 1, 1, 0, 1, 1): "5",
    (1, 0, 1, 1, 1, 1, 1): "6",
    (1, 1, 1, 0, 0, 0, 0): "7",
    (1, 1, 1, 1, 1, 1, 1): "8",
    (1, 1, 1, 1, 0, 1, 1): "9",
}

def recognize_digit(segment):
    segment = cv2.resize(segment, (3, 5))  # Resize segment to match template size

    best_match = None
    best_similarity = -1

    for pattern, digit in DIGITSDICT.items():
        similarity = cv2.matchShapes(segment, pattern, cv2.CONTOURS_MATCH_I1, 0)
        print(similarity)
        if best_match is None or similarity < best_similarity:
            best_match = digit
            best_similarity = similarity

    return best_match


img = cv2.imread('files/06.JPEG')
gry = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gry = cv2.threshold(gry, 80, 255, cv2.THRESH_BINARY)[1]
cv2.imshow('img', gry)
cv2.waitKey(0)

print(recognize_digit(gry))