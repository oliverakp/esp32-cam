from Google import Create_Service
import pandas as pd 
import time
from Download import download
import cv2
import pytesseract
import numpy as np
from utils import *

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'


CLIENT_SECRET_FILE = 'Client_secret.json'
API_NAME = 'drive'
API_VERSION = 'v3'
SCOPES = ['https://www.googleapis.com/auth/drive']

# service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

# folder_id = "12WNGNBwJFix14MvM4iG0XB8eKin-M9Do"
# query = f"parents = '{folder_id}'"

while True:
    # response = service.files().list(q=query).execute()
    # files = response.get('files')
    # nextPageToken = response.get('nextPageToken')
    
    # df = pd.DataFrame(files)
    
    # file_ids =[df["id"][0]]
    # timestamp = time.strftime("%Y%m%d-%H%M%S")
    # file_names =['esp32-' + timestamp + '.jpeg']
    
    # print(file_ids)

    # download(file_ids, file_names)
    
    # recongize_value('files/esp32-' + timestamp + '.JPEG')
    
    recongize_value('files/esp32-20230518-154312.JPEG')
    
    time.sleep(5)
    