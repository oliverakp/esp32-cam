import cv2
import pytesseract
import numpy as np
from utils import *

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'

#img = cv2.imread('orex-scc/IMG_20230324_125728_553.JPG')
img = cv2.imread('files/esp32-20230513-221005.JPEG')
img = cv2.resize(img, (800, 600))
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# (x1, y1), (x2, y2)
x1, y1, x2, y2 = 332, 190 ,372, 278
cv2.rectangle(gray, (x1, y1), (x2, y2), (0, 0, 255), 2)
cv2.imshow('img', gray)
cv2.waitKey(0)

# ---------------
cv2.rectangle(img, (420, 295), (435, 308), (0, 0, 255), 2)
cv2.imshow('img', img)
cv2.waitKey(0)

point = cut_segment(gray,300,304,425,430, 'point')
contains_point = count_gray(point)
# Define the rectangular area where the text will be extracted

"""digit1 = recognize_digit(gray, 335, 375, 220, 308)
digit2 = recognize_digit(gray, 385, 425, 225, 305)
digit3 = recognize_digit(gray, 435, 475, 225, 308)"""

digit1 = recognize_digit(gray, x1, x2, y1, y2)
digit2 = recognize_digit(gray, x1+50, x2+50, y1, y2)
digit3 = recognize_digit(gray, x1+100, x2+100, y1, y2)

if contains_point == 1:
    value = float(str(digit1) + str(digit2) + '.' + str(digit3))
else:
    value = float(str(digit1) + str(digit2) + str(digit3))
print(value)
