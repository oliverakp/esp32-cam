import cv2
import pytesseract
import numpy as np
from utils import *

pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'

img = cv2.imread('files/esp32-20230513-123522.JPEG')
img = cv2.resize(img, (800, 600))
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# ---------------
x1, y1, x2, y2 = 380, 220, 430, 308
#cv2.rectangle(gray, (x1, y1), (x2, y2), (0, 0, 255), 2)
#cv2.imshow('thresh', gray)
#cv2.waitKey(0)

rect = gray[y1:y2, x1:x2]
thresh = cv2.threshold(rect, 140, 255, cv2.THRESH_BINARY)[1]

cv2.imshow('thresh', thresh)
cv2.waitKey(0)
cv2.imwrite('files/processing/thresh.jpg', thresh)

digit_regions = segment_digits(thresh)

# Initialize an empty string to store the recognized digits
recognized_digits = ""

# Iterate through the digit regions and perform OCR
for region in digit_regions:
    # Apply OCR to the digit region
    digit = pytesseract.image_to_string(region, config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789')

    # Append the recognized digit to the final string
    recognized_digits += digit

print("Recognized digits:", recognized_digits)



"""cv2.rectangle(gray, (x1, y1), (x1 + 15, y1+45), (0, 255, 0), 2) # f
cv2.rectangle(gray, (x1, y1+50), (x1+15, y2), (0, 255, 0), 2)  # e
cv2.rectangle(gray, (x2 - 15, y1+50), (x2, y2), (0, 255, 0), 2)  # c
cv2.rectangle(gray, (x2 - 15, y1), (x2, y1+45), (0, 255, 0), 2)  # b
cv2.rectangle(gray, (x1, y1), (x2, y1+20), (0, 255, 0), 2)  # a
cv2.rectangle(gray, (x1, y1+70), (x2, y1+70), (0, 255, 0), 2)  # d
cv2.rectangle(gray, (x1, y1+37), (x2, y1+53), (0, 255, 0), 2)  # g

cv2.imshow('img', gray)
cv2.waitKey(0)


segments = [
    cut_segment(gray,y1,y1+20,x1,x2,'a'),
    cut_segment(gray,y1+10,y1+40,x2-10,x2,'b'),
    cut_segment(gray,y1+55,y2-5,x2-15,x2-5,'c'),
    cut_segment(gray,y2-15,y2-5,x1+5,x2-10,'d'),
    cut_segment(gray,y1+50,y2,x1,x1+15,'e'),
]

letters =['a', 'b', 'c', 'd', 'e', 'f', 'g']

for segment in segments:
    try:
        print(count_gray(segment))
    except:
        pass

DIGITSDICT = {
    (1, 1, 1, 1, 1, 1, 0): 0,
    (0, 1, 1, 0, 0, 0, 0): 1,
    (1, 1, 0, 1, 1, 0, 1): 2,
    (1, 1, 1, 1, 0, 0, 1): 3,
    (0, 1, 1, 0, 0, 1, 1): 4,
    (1, 0, 1, 1, 0, 1, 1): 5,
    (1, 0, 1, 1, 1, 1, 1): 6,
    (1, 1, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9,
}




"""

