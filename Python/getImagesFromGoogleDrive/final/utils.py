import cv2
import pytesseract
from scipy.spatial import distance


def cut_segment(image,y1,y2,x1,x2,letter):
    rect = image[y1:y2, x1:x2]
    thresh = cv2.threshold(rect, 80, 255, cv2.THRESH_BINARY)[1]
    #cv2.imshow('image', image)
    #cv2.waitKey(0)

    cv2.imwrite('files/processing/' + letter + '.jpg', thresh)
    return cv2.imread('files/processing/' + letter + '.jpg')

def is_black_greater_than_white(image):
    # Count the number of black and white pixels
    num_black_pixels = cv2.countNonZero(image)
    num_white_pixels = image.size - num_black_pixels

    # Compare the counts and return the result
    if num_black_pixels > num_white_pixels:
        print(num_black_pixels)
        return True
    else:
        print(num_black_pixels)
        print("dsfds")
        print(num_white_pixels)
        return False



def count_gray(image):

    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Iterate over each pixel and print its gray value
    height, width = gray.shape
    """cv2.imshow('img', gray)
    cv2.waitKey(0)"""
    countBlack = 0
    countWhite = 0
    for y in range(height):
        for x in range(width):
            pixel_value = gray[y, x]
            if gray[y,x] > 0 :
                countWhite += 1
            else:
                countBlack += 1
            #print(f"Pixel at ({x}, {y}) - Gray value: {pixel_value}")
    if countBlack > countWhite:
        return 1
    else:
        return 0

def segment_digits(thresh):

    # Find contours in the thresholded image
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    digit_regions = []

    # Iterate through the contours
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)

        # Filter out contours that are too small or too large
        if w < 10 or h < 10 or w > 200 or h > 200:
            continue

        # Extract the region of interest (ROI) containing the digit
        digit_roi = thresh[y:y + h, x:x + w]

        # Append the ROI to the list of digit regions
        digit_regions.append(digit_roi)

    return digit_regions

def recognize_digit(gray, x1, x2, y1, y2):
    cv2.rectangle(gray, (x1, y1), (x2, y2), (0, 255, 0), 2)

    """cv2.rectangle(gray, (x1, y1), (x1 + 15, y1+45), (0, 255, 0), 2) # f
    cv2.rectangle(gray, (x1, y1+50), (x1+15, y2), (0, 255, 0), 2)  # e
    cv2.rectangle(gray, (x2 - 15, y1+50), (x2, y2), (0, 255, 0), 2)  # c
    cv2.rectangle(gray, (x2 - 15, y1), (x2, y1+45), (0, 255, 0), 2)  # b
    cv2.rectangle(gray, (x1, y1), (x2, y1+20), (0, 255, 0), 2)  # a
    cv2.rectangle(gray, (x1, y1+70), (x2, y1+70), (0, 255, 0), 2)  # d
    cv2.rectangle(gray, (x1, y1+37), (x2, y1+53), (0, 255, 0), 2)  # g"""

    segments = [
        cut_segment(gray, y1, y1 + 10, x1 + 10, x2, 'a'),
        cut_segment(gray, y1 + 10, y1 + 40, x2 - 10, x2, 'b'),
        cut_segment(gray, y1 + 55, y2 - 5, x2 - 15, x2 - 5, 'c'),
        cut_segment(gray, y2 - 8, y2 - 5, x1 + 10, x2 - 15, 'd'),
        cut_segment(gray, y1 + 50, y2, x1, x1 + 15, 'e'),
        cut_segment(gray, y1, y1 + 45, x1 + 12, x1 + 15, 'f'),
        cut_segment(gray, y1 + 42, y1 + 46, x1 + 10, x2 - 10, 'g'),
    ]

    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

    list = []
    for segment in segments:
        try:
            # print(count_gray(segment))
            list.append(count_gray(segment))
        except:
            pass

    DIGITSDICT = {
        (0, 0, 0, 0, 0, 0, 0): " ",
        (1, 1, 1, 1, 1, 1, 0): "0",
        (0, 1, 1, 0, 0, 0, 0): "1",
        (1, 1, 0, 1, 1, 0, 1): "2",
        (1, 1, 1, 1, 0, 0, 1): "3",
        (0, 1, 1, 0, 0, 1, 1): "4",
        (1, 0, 1, 1, 0, 1, 1): "5",
        (1, 0, 1, 1, 1, 1, 1): "6",
        (1, 1, 1, 0, 0, 0, 0): "7",
        (1, 1, 1, 1, 1, 1, 1): "8",
        (1, 1, 1, 1, 0, 1, 1): "9",
    }

    digit_key = tuple(list)
    try:
        digit = DIGITSDICT[digit_key]
    except:
        most_similar_key = find_most_similar_key(digit_key, DIGITSDICT)
        digit = DIGITSDICT[most_similar_key]

    return digit

def find_most_similar_key(target_key, dictionary):
    most_similar_key = None
    highest_similarity = float('-inf')

    for key in dictionary.keys():
        similarity = distance.hamming(target_key, key)
        if similarity > highest_similarity:
            highest_similarity = similarity
            most_similar_key = key

    return most_similar_key