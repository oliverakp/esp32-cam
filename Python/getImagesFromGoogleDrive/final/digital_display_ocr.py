import numpy as np
import cv2
import imutils
from skimage import exposure
from pytesseract import image_to_string
import PIL

def take_picture(should_save=False, d_id=0):
    cam = cv2.VideoCapture(d_id)
    s, img = cam.read()
    if s:
        if should_save:
            cv2.imwrite('ocr.jpg', img)
        print("picture taken")
    return img


def cnvt_edged_image(img_arr, should_save=False):
    image = imutils.resize(img_arr, height=300)
    gray_image = cv2.bilateralFilter(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY), 11, 17, 17)
    edged_image = cv2.Canny(gray_image, 30, 200)

    if should_save:
        cv2.imwrite('cntr_ocr.jpg', edged_image)

    return edged_image


def find_display_contour(edge_img_arr):
    display_contour = None
    edge_copy = edge_img_arr.copy()
    contours, hierarchy = cv2.findContours(edge_copy, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    top_cntrs = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

    for cntr in top_cntrs:
        peri = cv2.arcLength(cntr, True)
        approx = cv2.approxPolyDP(cntr, 0.02 * peri, True)

        if len(approx) == 4:
            display_contour = approx
            break

    return display_contour


def crop_display(image_arr):
    edge_image = cnvt_edged_image(image_arr)
    display_contour = find_display_contour(edge_image)
    cntr_pts = display_contour.reshape(4, 2)
    return cntr_pts


def normalize_contrs(img, cntr_pts):
    ratio = img.shape[0] / 300.0
    norm_pts = np.zeros((4, 2), dtype="float32")

    s = cntr_pts.sum(axis=1)
    norm_pts[0] = cntr_pts[np.argmin(s)]
    norm_pts[2] = cntr_pts[np.argmax(s)]

    d = np.diff(cntr_pts, axis=1)
    norm_pts[1] = cntr_pts[np.argmin(d)]
    norm_pts[3] = cntr_pts[np.argmax(d)]

    norm_pts *= ratio

    (top_left, top_right, bottom_right, bottom_left) = norm_pts

    width1 = np.sqrt(((bottom_right[0] - bottom_left[0]) ** 2) + ((bottom_right[1] - bottom_left[1]) ** 2))
    width2 = np.sqrt(((top_right[0] - top_left[0]) ** 2) + ((top_right[1] - top_left[1]) ** 2))
    height1 = np.sqrt(((top_right[0] - bottom_right[0]) ** 2) + ((top_right[1] - bottom_right[1]) ** 2))
    height2 = np.sqrt(((top_left[0] - bottom_left[0]) ** 2) + ((top_left[1] - bottom_left[1]) ** 2))

    max_width = max(int(width1), int(width2))
    max_height = max(int(height1), int(height2))

    dst = np.array([[0, 0], [max_width - 1, 0], [max_width - 1, max_height - 1], [0, max_height - 1]], dtype="float32")

